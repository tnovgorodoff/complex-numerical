#include <iostream>
using namespace std;

class Complex //реализуем класс для операций над комплексными числами
{
    private:
        double real, imag;

    public:
        Complex()
        {
            real = imag = 0;
        }
        Complex(double r)
        {
            real = r;
            imag = 0;
        } 
        Complex(double r, double i)
        {
            real = r;
            imag = i;
        }   
        Complex(Complex &obj) //берем реальную и мнимую часть объекта, вводимого с клавиатуры
        {
            real = obj.real;
            imag = obj.imag;
        }
        Complex add(Complex c) //операция сложения комплексного числа
        {
            Complex Add;
            Add.real = real + c.real;
            Add.imag = imag + c.imag;
            return Add;
        }
        Complex sub(Complex c) //опреация вычитания комплексного числа
        {
            Complex Sub;
            Sub.real = real - c.real;
            Sub.imag = imag - c.imag;
            return Sub;
        }
        Complex mult(Complex c) //операция умножения комплексного числа
        {
            Complex Mult;
            Mult.real = real*c.real - imag*c.imag;
            Mult.imag = real*c.imag - c.real*imag;
            return Mult;
        }
        Complex div(Complex c) //операция деления комплексного числа
        {
            Complex Div;
            Div.real = (real*c.real + imag*c.imag)/(c.real*c.real + c.imag*c.imag);
            Div.imag = (imag*c.real + real*c.imag)/(c.real*c.real + c.imag*c.imag);
            return Div;
        }

        void print() //вывод на экран мнимой части с i
        {
            cout<<real<<"+"<<imag<<"i"<<endl<<endl;
        }
        //возвращаем и присваиваем комплекснуюи мнимые части
        double getReal() const
        {
            return real;
        }
        double getImag() const
        {
            return imag;
        }
        void setReal(double re)
        {
            real = re;
        }
        void setImag(double im)
        {
            imag = im;
        }

};

int main()
{
    double real1,imag1,real2,imag2;

    cout<<"Введите реальную часть первого комплексного числа: ";
        cin>>real1;
    cout<<"Введите мнимую часть первого комплексного числа: ";
        cin>>imag1;
    Complex obj1(real1,imag1);
    obj1.print();

    cout<<"Введите реальную часть второго комплексного числа: ";
        cin>>real2;
    cout<<"Введите мнимую часть второго комплексного числа: ";
        cin>>imag2;
    Complex obj2(real2,imag2);
    obj2.print();

    Complex c;
    c = obj1.add(obj2);
    cout<<"Результат сложения = ("<<c.getReal()<<")+("<<c.getImag()<<")i"<<endl;

    c = obj1.sub(obj2);
    cout<<endl<<"Результат вычитания = ("<<c.getReal()<<")+("<<c.getImag()<<")i"<<endl;

    c = obj1.mult(obj2);
    cout<<endl<<"Результат умножения = ("<<c.getReal()<<")+("<<c.getImag()<<")i"<<endl;

    c = obj1.div(obj2);
    cout<<endl<<"Результат деления = ("<<c.getReal()<<")+("<<c.getImag()<<")i"<<endl;
    return 0;
}